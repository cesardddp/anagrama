from flask import Flask, render_template, request
from DDL import inicia,fixa,apnd
import lista
import re




app = Flask(__name__)

coon = inicia()

@app.route('/', methods=['GET', 'POST'])
def principal():

    nome_req = request.args.get('nome') 
    if not nome_req:
        return render_template('temp.html', nome='')
    else:
        nome_req = re.sub(u'[^a-zA-Z0-9áéíóúÁÉÍÓÚâêîôÂÊÎÔãõÃÕçÇ: ]', '', nome_req)
        apnd(coon,str(nome_req))
        lista3 = lista.sorteia(str(nome_req))
        return render_template('temp.html', lista_templ=lista3, nome=nome_req)



if __name__ == "__main__":
    app.run()
    fixa(coon)
    



